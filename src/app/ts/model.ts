import {StringFormatType, Strings} from './strings';

export interface IModelObject {
  name?: string;
  nodes: IModelNode[];
}

export interface IModelNode {
  name?: string; // Name of the config item
  description?: string; // Description of the config item
  path?: string; // Path to the final object
  default?: any; // Default value if undefined
  mandatory?: boolean; // is mandatory or not
  type?: ModelNodeType; // Typeof val expected
  controlType?: ModelNodeControlType; // Typeof val expected
  possibleValues?: IModelNodeValues[]; // if passed, only those values will be possible
  validator?: (entry) => boolean; // Validates or not the value passed
  transformType?: (entry: any) => any; // Transform value if not the type expected.
  transformValue?: (entry: any) => any; // Transform based on value (entry corresponds only to the current value)
  transformItem?: (entry: any, object: any) => any; // Transform based on item (entry corresponds to whole object)
  hideInput?: boolean; // if true, input should be hidden for the user
  hideDisk?: boolean; // hide this node on disk?
  childs?: IModelObject; // child nodes
}

export interface IModelNodeValues {
  name: string;
  value: any;
}

export enum ModelNodeType {
  String = 'string',
  Number = 'number',
  Boolean = 'boolean',
  Object = 'object'
}

export enum ModelNodeControlType {
  String,
  Number,
  Checkbox,
  Select,
  Browse,
}

function mergeModelObjects(models: IModelObject[]): IModelNode[] {
  const data = [];
  data.push(...models.map(m => m.nodes));
  return data;
}

export function validateObject(object: any, model: IModelObject): boolean {
  return true;
}

export function compactObject(object: any, model: IModelObject): any {
  const finalObject = {};

  for (const item of model.nodes) {
    if (!item.hideDisk) {
      if (item.childs) {
        finalObject[item.path] = compactObject(object[item.path], item.childs);
      } else {
        finalObject[item.path] = object[item.path];
      }
    }
  }

  return finalObject;
}

function correctNode(val: any, object: any, model: IModelNode): any {
  // Val is present.
  if (val !== undefined) {
    // Verify the type of val
    if (model.type && !verifyType(val, model.type)) {
      if (model.transformType) { // Wrong type, try to transform it
        val = model.transformType(val);
      } else {
        return null;
      }
    }
  } else {
    if (model.default !== undefined) {
      val = model.default;
    } else if (model.mandatory) {
      return null;
    }
  }

  if (model.transformValue) {
    val = model.transformValue(val);
  }

  if (model.transformItem) {
    val = model.transformItem(val, object);
  }

  if (model.validator && !model.validator(val)) {
    return null;
  }

  return val;
}

export function correctObject(object: any = {}, model: IModelObject): any {
  const finalObject = {};

  for (const node of model.nodes) {
    if (node.childs) {
      finalObject[node.path] = correctObject(object[node.path] || {}, node.childs);
    } else {
      const val = correctNode(object[node.path], finalObject, node);

      if (val !== null) {
        finalObject[node.path] = val;
      }
    }
  }

  return finalObject;
}

function verifyType(val: any, type: ModelNodeType): boolean {
  return typeof val === type.toString();
}

export function prepareNode(model: IModelNode): boolean {
  if (model.name && !model.path) {
    model.path = Strings.transform(model.name, StringFormatType.CamelCase);
  } else if (model.path && !model.name) {
    model.name = Strings.transform(model.path, StringFormatType.Humanize);
  } else if (!model.name && !model.path) {
    return false;
  }

  if (model.default) {
    if (model.type && !verifyType(model.default, model.type)) {
      return false;
    }
  }

  if (model.possibleValues) {
    for (const p of model.possibleValues) {
      if (model.type && !verifyType(p.value, model.type)) {
        return false;
      }
    }
  }

  if (!model.controlType && model.type) {
    switch (model.type) {
      case ModelNodeType.String: model.controlType = ModelNodeControlType.String; break;
      case ModelNodeType.Number: model.controlType = ModelNodeControlType.Number; break;
      case ModelNodeType.Boolean: model.controlType = ModelNodeControlType.Checkbox; break;
    }
  }

  if (model.childs) {
    if (!prepareModel(model.childs)) {
      return false;
    }
  }

  return true;
}

export function prepareModel(model: IModelObject): boolean {
  let success = true;
  for (const nodes of model.nodes) {
    success = success && prepareNode(nodes);
  }
  return success;
}
