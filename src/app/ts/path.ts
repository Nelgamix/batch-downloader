export function writePath(obj: any, path: string, data: any, separator = '.'): boolean {
  const tokens = path.split(separator);
  let fo = obj;
  let i;
  for (i = 0; i < tokens.length - 1; i++) {
    const token = tokens[i];
    if (fo[token] === undefined) {
      fo[token] = {};
    }

    fo = fo[token];
  }

  if (fo) {
    fo[tokens[i]] = data;
  }

  return true;
}

export function readPath(obj: any, path: string, separator = '.'): any {
  const tokens = path.split(separator);
  let fo = obj;
  for (const token of tokens) {
    if (fo[token]) {
      fo = fo[token];
    } else {
      return undefined;
    }
  }
  return fo;
}
