export enum StringFormatType {
  CamelCase,
  KebabCase,
  Humanize,
}

export class Strings {
  static transform(val: any, to: StringFormatType): string {
    const valS = typeof val === 'string' ? val : String(val);

    switch (to) {
      case StringFormatType.CamelCase:
        return this.transformFirstLetter(valS.split(' ').map(s => this.capitalize(s)).join(''), s => s.toLowerCase());
      case StringFormatType.KebabCase:
        return valS.split(' ').map(s => this.capitalize(s)).join('');
      case StringFormatType.Humanize:
        return valS;
    }
  }

  private static capitalize(string: string): string {
    return this.transformFirstLetter(string.toLowerCase(), s => s.toUpperCase());
  }

  private static transformFirstLetter(string: string, fn: (s: string) => string): string {
    return fn(string[0]) + string.substring(1);
  }
}
