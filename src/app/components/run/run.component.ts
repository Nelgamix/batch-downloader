import { Component, OnInit } from '@angular/core';
import {ConfigurationService, IConfiguration, IConfigurationTarget} from '../../providers/configuration.service';

@Component({
  selector: 'app-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent implements OnInit {
  configs: IConfiguration[];
  configSelected: IConfiguration;
  targetSelected: IConfigurationTarget[];

  constructor(private configurationService: ConfigurationService) { }

  ngOnInit() {
    this.configs = this.configurationService.configs;
  }

  selectConfig(config: IConfiguration) {
    this.configSelected = config;
    this.targetSelected = config.target;
  }

  selectTarget(config: IConfiguration, target: IConfigurationTarget) {
    this.configSelected = config;
    this.targetSelected = [target];
  }
}
