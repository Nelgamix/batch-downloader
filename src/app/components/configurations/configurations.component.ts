import { Component, OnInit } from '@angular/core';
import {
  configModel,
  ConfigurationService,
  IConfiguration,
  IConfigurationTarget,
  targetModel
} from '../../providers/configuration.service';
import {SourceService} from '../../providers/source.service';
import {correctObject, IModelNodeValues} from '../../ts/model';
import {IItemChanged} from '../form/form.component';
import {ISourceType} from '../../providers/bootstrap.service';

@Component({
  selector: 'app-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.scss']
})
export class ConfigurationsComponent implements OnInit {
  configs: IConfiguration[];
  configSelected: IConfiguration;
  configModel = configModel;

  targets: IConfigurationTarget[];
  targetSelected: IConfigurationTarget;
  targetModel = targetModel;
  targetArgsModel;

  sources: IModelNodeValues[];

  constructor(private configuration: ConfigurationService, private sourcesService: SourceService) { }

  ngOnInit() {
    this.configs = this.configuration.configs;
    this.sources = this.sourcesService.modulesTypes.map(s => {
      return {
        name: s,
        value: s
      };
    });
  }

  selectConfig(config: IConfiguration) {
    this.configSelected = config;
    this.targets = config.target;
    this.targetSelected = undefined;
    this.targetArgsModel = undefined;
  }

  selectTarget(target: IConfigurationTarget) {
    this.targetSelected = target;
    this.targetModel.nodes.find(e => e.path === 'sourceType').possibleValues = this.sources;

    if (this.targetSelected.sourceType) {
      this.sourceChanged(this.targetSelected.sourceType);
    } else {
      this.targetArgsModel = undefined;
    }
  }

  targetChanged(event: IItemChanged) {
    if (event.path === 'sourceType') {
      this.sourceChanged(event.value);
    }
  }

  sourceChanged(type: ISourceType) {
    if (!type) {
      this.targetArgsModel = undefined;
      return;
    }

    const instances = this.sourcesService.getInstancesWithType(type);
    this.targetModel.nodes.find(e => e.path === 'sourceId').possibleValues = instances.map(i => {
      return {
        name: i.name,
        value: i.name
      };
    });

    const module = this.sourcesService.getModuleWithType(type);
    this.targetArgsModel = module.configItems;
    this.targetSelected.options = correctObject({}, this.targetArgsModel);
  }

  newTarget() {
    if (!this.configSelected.target) {
      this.configSelected.target = [];
    }

    const finalObject = correctObject({name: 'Target ' + this.configSelected.target.length}, this.targetModel);
    this.configSelected.target.push(finalObject);
    this.targets = this.configSelected.target;
  }

  newConfig() {
    this.configuration.addConfig();
  }

  saveConfig() {
    this.configuration.saveConfig(this.configSelected.id);
  }

  deleteConfig() {
    if (confirm('Are you sure you want to delete this config?')) {
      this.configuration.deleteConfig(this.configSelected.id);
    }
  }
}
