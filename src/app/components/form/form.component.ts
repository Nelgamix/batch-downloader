import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {distinctUntilChanged} from 'rxjs/operators';
import {IModelNode, IModelObject, ModelNodeControlType, ModelNodeType} from '../../ts/model';
import {writePath} from '../../ts/path';
import {ElectronService} from '../../providers/electron.service';

export interface IItemChanged {
  path: string;
  value: any;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnChanges {
  @Input() model: IModelObject;
  @Input() item: any;
  @Input() parent: IModelNode;

  @Output() itemChanged = new EventEmitter<IItemChanged>();

  form: FormGroup;

  ModelNodeControlType = ModelNodeControlType;

  constructor(private fb: FormBuilder, private electron: ElectronService) { }

  ngOnInit() {
    this.ngOnChanges();
  }

  ngOnChanges() {
    const group = {};

    for (const node of this.model.nodes) {
      switch (node.type) {
        case ModelNodeType.String:
          group[node.path] = [this.item[node.path]];
          break;
        case ModelNodeType.Number:
          group[node.path] = [this.item[node.path]];
          break;
        case ModelNodeType.Boolean:
          group[node.path] = [(this.item[node.path] ? true : false)];
          break;
      }
    }

    this.form = this.fb.group(group);
    for (const c of Object.keys(this.form.controls)) {
      this.form.controls[c].valueChanges
        .pipe(distinctUntilChanged())
        .subscribe((nv) => this.onItemChanged({
          path: c,
          value: nv
        }));
    }
  }

  onItemChanged(change: IItemChanged) {
    if (this.parent) {
      change.path = this.parent.path + '.' + change.path;
    } else {
      this.changeConfig(change.path, change.value);
    }

    this.itemChanged.emit(change);
  }

  possibleValueChange(item, event) {
    this.onItemChanged({
      path: item.path,
      value: event ? event.value : undefined
    });
  }

  browseChanged(dir: string, itemPath: string) {
    if (this.electron.fs.existsSync(dir)) {
      this.form.controls[itemPath].patchValue(dir);
      this.onItemChanged({
        path: itemPath,
        value: dir
      });
    }
  }

  openDialog(itemPath: string) {
    const item = this.item[itemPath];
    const files = this.electron.dialog.showOpenDialog({ properties: ['openDirectory'], defaultPath: item });
    if (files) {
      this.browseChanged(files[0], itemPath);
    }
  }

  private changeConfig(path: string, val) {
    console.log('Write at', path, val);
    writePath(this.item, path, val);
  }
}
