import { Component, OnInit } from '@angular/core';
import {ISource, SourceService} from '../../providers/source.service';
import {Source} from '../../providers/sources/source';
import {ISourceType} from '../../providers/bootstrap.service';
import {IModelObject} from '../../ts/model';

@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.scss']
})
export class SourcesComponent implements OnInit {
  modules: Source[];
  modulesNames: ISourceType[];
  instances: ISource[];

  instanceSelected: ISource;
  instanceSelectedModel: IModelObject;

  constructor(private sources: SourceService) { }

  ngOnInit() {
    this.modules = this.sources.modules;
    this.modulesNames = this.sources.modulesTypes;
    this.instances = this.sources.instances;
  }

  getInstancesFor(type: ISourceType): ISource[] {
    return this.sources.getInstancesWithType(type);
  }

  newInstance(type: ISourceType) {
    this.sources.addInstance(type);
  }

  selectInstance(instance: ISource) {
    this.instanceSelected = instance;
    this.instanceSelectedModel = this.sources.getModuleWithType(instance.type).sourceItems;
  }

  saveInstance() {
    this.sources.saveInstance(this.instanceSelected.id);
  }
}
