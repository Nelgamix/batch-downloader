import {Injectable} from '@angular/core';
import {ElectronService} from './electron.service';
import * as glob from 'glob';
import {IConfigurationTargetOptions, ISourceType} from './bootstrap.service';
import {compactObject, correctObject, IModelObject, ModelNodeControlType, ModelNodeType} from '../ts/model';
import {StringFormatType, Strings} from '../ts/strings';

export interface IConfigurationOptionsDownload {
  parallel: number;
  max: number;
}

export interface IConfigurationOptions {
  download: IConfigurationOptionsDownload;
}

export interface IConfigurationTarget {
  name: string;
  sourceType: ISourceType;
  sourceId: string;
  path?: string;
  wipe?: boolean;
  options?: IConfigurationTargetOptions;
}

export interface IConfiguration {
  name: string;
  id: string;
  path: string;
  target?: IConfigurationTarget[];
  options?: IConfigurationOptions[];
}

export const configModel: IModelObject = {
  nodes: [
    {
      name: 'Name',
      type: ModelNodeType.String
    },
    {
      name: 'Id',
      type: ModelNodeType.String,
      transformItem: (entry, object) => entry ? entry : Strings.transform(object.name, StringFormatType.CamelCase)
    },
    {
      name: 'Path',
      type: ModelNodeType.String,
      controlType: ModelNodeControlType.Browse,
      transformItem: (entry, object) => entry ? entry : object.id
    },
    {
      name: 'Target',
      default: [],
      hideInput: true
    },
    {
      name: 'Options',
      childs: {
        nodes: [
          {
            name: 'Download',
            childs: {
              nodes: [
                {
                  name: 'Parallel',
                  type: ModelNodeType.Number,
                  default: 5
                },
                {
                  name: 'Max',
                  type: ModelNodeType.Number,
                  default: 0
                }
              ]
            }
          }
        ]
      }
    }
  ]
};

export const targetModel: IModelObject = {
  nodes: [
    {
      name: 'Name',
      type: ModelNodeType.String
    },
    {
      name: 'Path',
      type: ModelNodeType.String,
      controlType: ModelNodeControlType.Browse,
      transformItem: (entry, object) => entry ? entry : Strings.transform(object.name, StringFormatType.CamelCase)
    },
    {
      name: 'Source Type',
      type: ModelNodeType.String,
      controlType: ModelNodeControlType.Select,
      path: 'sourceType',
      possibleValues: []
    },
    {
      name: 'Source Config',
      type: ModelNodeType.String,
      controlType: ModelNodeControlType.Select,
      path: 'sourceId',
      possibleValues: []
    }
  ]
};

@Injectable()
export class ConfigurationService extends ElectronService {
  configs: IConfiguration[];

  private folderPath = './configs';

  constructor() {
    super();

    this.configs = this.parseConfigurations(this.folderPath);
  }

  parseConfigurations(path: string): IConfiguration[] {
    if (!this.fs.existsSync(path)) {
      this.fs.mkdirSync(path);
      return [];
    }

    const configs: IConfiguration[] = [];
    const matches = glob.sync(path + '/*.config.json');

    for (const match of matches) {
      const config: IConfiguration = JSON.parse(this.fs.readFileSync(match, {encoding: 'utf-8'}));
      configs.push(config);
    }

    return configs;
  }

  saveConfig(id: string): void {
    const config = this.getConfig(id);

    if (config) {
      const path = `${this.folderPath}/${config.id}.config.json`;
      const compacted = compactObject(config, configModel);
      const data = JSON.stringify(compacted, null, 4);
      this.fs.writeFileSync(path, data, {encoding: 'utf-8'});
    }
  }

  deleteConfig(id: string) {
    const config = this.getConfig(id);

    if (config) {
      const path = `${this.folderPath}/${config.id}.config.json`;

      if (this.fs.existsSync(path)) {
        this.fs.unlinkSync(path);
        this.configs.splice(this.configs.findIndex(c => config.id === c.id), 1);
      }
    }
  }

  getConfig(id: string): IConfiguration {
    return this.configs.find(c => c.id === id);
  }

  addConfig(name?: string): void {
    const finalObject = correctObject({name: name || ('Config ' + this.configs.length)}, configModel);
    this.configs.push(finalObject);
  }
}
