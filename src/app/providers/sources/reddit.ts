import {Source} from './source';
import {IIndex, IItemPreview} from '../index.service';
import {from, Observable, of} from 'rxjs';
import * as Snoowrap from 'snoowrap';
import {Comment, Listing, Submission} from 'snoowrap';
import {map} from 'rxjs/operators';
import {ISourceType} from '../bootstrap.service';
import {IModelObject, ModelNodeControlType, ModelNodeType} from '../../ts/model';

export interface IConfigurationTargetOptionsRedditGroup {
  nsfw?: boolean;
}

export interface IConfigurationTargetOptionsRedditHide {
  nsfw?: boolean;
}

export interface IConfigurationTargetOptionsReddit {
  type: 'saved' | 'upvoted';
  group: IConfigurationTargetOptionsRedditGroup;
  hide: IConfigurationTargetOptionsRedditHide;
}

export interface ISourceReddit {
  userAgent: string;
  clientId: string;
  clientSecret: string;
  username: string;
  password: string;
}

export interface IItemMetadataReddit {
  id: string;
  url: string;
  name: string;
  subreddit: string;
  subredditName: string;
  subredditSubscribers: number;
  domain: string;
  author: string;
  authorName: string;
  title: string;
  text: string;
  score: number;
  postHint: string;
  nsfw: boolean;
  permalink: string;
  createdUtc: number;
}

const sourceItems: IModelObject = {
  nodes: [
    {
      name: 'User Agent',
      type: ModelNodeType.String
    },
    {
      name: 'Client ID',
      type: ModelNodeType.String
    },
    {
      name: 'Client Secret',
      type: ModelNodeType.String
    },
    {
      name: 'Username',
      type: ModelNodeType.String
    },
    {
      name: 'Password',
      type: ModelNodeType.String
    }
  ]
};

const configItems: IModelObject = {
  nodes: [
    {
      name: 'Type',
      type: ModelNodeType.String,
      controlType: ModelNodeControlType.Select,
      possibleValues: [
        {name: 'Upvoted', value: 'upvoted'},
        {name: 'Saved', value: 'saved'}
      ]
    },
    {
      name: 'Hide',
      childs: {
        nodes: [
          {
            name: 'NSFW',
            type: ModelNodeType.Boolean
          },
        ]
      }
    },
    {
      name: 'Group',
      childs: {
        nodes: [
          {
            name: 'NSFW',
            type: ModelNodeType.Boolean
          },
        ]
      }
    },
  ]
};

export type SourceTypeReddit = 'Reddit';

export class RedditSource extends Source {
  name: ISourceType = 'Reddit';
  sourceItems = sourceItems;
  configItems = configItems;

  private wrapper: Snoowrap;

  constructor() {
    super();
  }

  configure(args: ISourceReddit): Observable<boolean> {
    try {
      this.wrapper = new Snoowrap(args);
      return of(true);
    } catch (e) {
      return of(false);
    }
  }

  analyse(args: IConfigurationTargetOptionsReddit, index: IIndex): Observable<IItemPreview[]> {
    return this.getObs().pipe(
      map((items) => this.transform(items))
    );
  }

  private transform(items: Listing<Comment | Submission>): IItemPreview[] {
    return items.map(it => {
      return {
        name: it.name,
        metadata: {}
      };
    });
  }

  private getObs(): Observable<Listing<Comment | Submission>> {
    return from(this.wrapper.getMe().getUpvotedContent() as any);
  }
}
