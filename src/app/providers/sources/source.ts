import {Observable} from 'rxjs';
import {IIndex, IItemPreview} from '../index.service';
import {ISourceType} from '../bootstrap.service';
import {IModelObject} from '../../ts/model';

export abstract class Source {
  abstract name: ISourceType;
  abstract sourceItems: IModelObject;
  abstract configItems: IModelObject;

  abstract configure(args: any): Observable<boolean>;
  abstract analyse(args: any, index: IIndex): Observable<IItemPreview[]>;
}
