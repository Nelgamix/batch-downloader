import {Injectable} from '@angular/core';
import {ElectronService} from './electron.service';
import {ISourceArgsType, ISourceType} from './bootstrap.service';
import * as glob from 'glob';
import {Source} from './sources/source';
import {compactObject, correctObject, IModelObject, ModelNodeType} from '../ts/model';
import {StringFormatType, Strings} from '../ts/strings';

export interface ISource {
  name: string;
  id: string;
  type: ISourceType;
  args: ISourceArgsType;
}

export const sourceModel: IModelObject = {
  name: 'Source',
  nodes: [
    {
      name: 'Name',
      type: ModelNodeType.String,
      mandatory: true
    },
    {
      name: 'Type',
      type: ModelNodeType.String,
      mandatory: true
    },
    {
      name: 'Id',
      type: ModelNodeType.String,
      transformItem: (entry, object) => entry ? entry : Strings.transform(object.name, StringFormatType.CamelCase)
    },
    {
      name: 'args'
    }
  ]
};

@Injectable({
  providedIn: 'root'
})
export class SourceService extends ElectronService {
  modules: Source[];
  instances: ISource[];

  private folderPath = './sources';

  constructor() {
    super();

    this.modules = [];
    this.instances = this.parseInstances(this.folderPath);
  }

  parseInstances(path: string): ISource[] {
    if (!this.fs.existsSync(path)) {
      this.fs.mkdirSync(path);
      return [];
    }

    const sources: ISource[] = [];
    const matches = glob.sync(path + '/*.source.json');

    for (const match of matches) {
      const source: ISource = JSON.parse(this.fs.readFileSync(match, {encoding: 'utf-8'}));
      sources.push(source);
    }

    return sources;
  }

  saveInstance(id: string) {
    const instance = this.getInstance(id);

    if (instance) {
      const path = `${this.folderPath}/${instance.id}.source.json`;
      const data = JSON.stringify(compactObject(instance, sourceModel), null, 4);
      this.fs.writeFileSync(path, data, {encoding: 'utf-8'});
    }
  }

  getInstance(id: string): ISource | undefined {
    return this.instances.find(i => i.id === id);
  }

  getInstancesWithType(type: ISourceType): ISource[] {
    return this.instances.filter(i => i.type === type);
  }

  addInstance(type: ISourceType, name?: string) {
    const args = correctObject({}, this.getModuleWithType(type).sourceItems);
    const finalName = name || ('Instance ' + this.instances.length);
    const finalObject = correctObject({
      name: finalName,
      type,
      args
    }, sourceModel);
    this.instances.push(finalObject);
  }

  get instancesName() {
    return this.instances.map(i => i.name);
  }

  addModule(source: Source): void {
    this.modules.push(source);
  }

  addModules(sources: Source[]): void {
    for (const s of sources) {
      this.addModule(s);
    }
  }

  getModuleWithType(type: ISourceType): Source {
    return this.modules.find(s => s.name === type);
  }

  get modulesTypes(): ISourceType[] {
    return this.modules.map(s => s.name);
  }
}
