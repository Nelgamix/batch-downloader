import {Injectable} from '@angular/core';
import {
  IConfigurationTargetOptionsReddit,
  IItemMetadataReddit, ISourceReddit,
  RedditSource,
  SourceTypeReddit
} from './sources/reddit';
import {sourceModel, SourceService} from './source.service';
import {configModel, targetModel} from './configuration.service';
import {prepareModel} from '../ts/model';

export type ISourceType = SourceTypeReddit;
export type ISourceArgsType = ISourceReddit;
export type IConfigurationTargetOptions = IConfigurationTargetOptionsReddit | {};
export type IItemMetadata = IItemMetadataReddit;

@Injectable({
  providedIn: 'root'
})
export class BootstrapService {
  constructor(private sources: SourceService) {
    prepareModel(configModel);
    prepareModel(targetModel);
    prepareModel(sourceModel);

    sources.addModules([
      new RedditSource()
    ]);

    sources.modules.forEach(s => {
      prepareModel(s.configItems);
      prepareModel(s.sourceItems);
    });
  }
}
