import {Injectable} from '@angular/core';
import {ElectronService} from './electron.service';
import {IItemMetadata} from './bootstrap.service';

export interface IItem {
  name: string;
  id: string;
  number: number;
  date: string;
  path: string;
  extension: string;
  downloader: any;
  source: any;
  metadata: IItemMetadata;
}

export interface IItemPreview {
  name: string;
  metadata: any;
}

export interface IItemFailed extends IItem {
  failId: number;
  failReason: string;
}

export interface IRun {
  date: string;
  numberOfItems: number;
  success: IItem[];
  fail: IItemFailed[];
}

export interface IIndex {
  dateCreated: string;
  dateUpdated: string;
  numberOfItems: number;
  runs: IRun[];
}

@Injectable({
  providedIn: 'root'
})
export class IndexService extends ElectronService {
  constructor() {
    super();
  }

  getIndex(path: string) {
    const ifile = path + '/' + this.getIndexName();
    if (this.fs.existsSync(ifile)) {
      const indexData = JSON.parse(this.fs.readFileSync(ifile, {encoding: 'utf-8'}));
      return indexData;
    }
  }

  getIndexName() {
    return '00000__index.json';
  }
}
