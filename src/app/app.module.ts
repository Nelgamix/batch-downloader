import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FormComponent } from './components/form/form.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {ConfigurationService} from './providers/configuration.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SourcesComponent } from './components/sources/sources.component';
import { ConfigurationsComponent } from './components/configurations/configurations.component';
import { RunComponent } from './components/run/run.component';
import {PropagationStopModule} from 'ngx-propagation-stop';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    FormComponent,
    SourcesComponent,
    ConfigurationsComponent,
    RunComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgSelectModule,
    NgbModule,
    PropagationStopModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ElectronService,
    ConfigurationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
