import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConfigurationsComponent} from './components/configurations/configurations.component';
import {SourcesComponent} from './components/sources/sources.component';
import {RunComponent} from './components/run/run.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomeComponent },
  { path: 'sources', component: SourcesComponent },
  { path: 'configurations', component: ConfigurationsComponent },
  { path: 'run', component: RunComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
